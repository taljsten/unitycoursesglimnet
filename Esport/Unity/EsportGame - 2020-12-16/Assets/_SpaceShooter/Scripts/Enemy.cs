﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 10;
    [SerializeField] private float randomDistance = 5;
    Vector3 dir = Vector3.back;


    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = Vector3.right *
            UnityEngine.Random.Range(-randomDistance, randomDistance);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += speed * dir * Time.deltaTime;
    }
}
