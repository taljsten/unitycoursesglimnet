﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour
{
    public float speed = 5;
    public float jumpForce = 100;
    Rigidbody rb;
    bool itsAlive;
    Vector3 startPos;
    RigidbodyConstraints startConstraints;

    // Start is called before the first frame update
    void Start()
    {
        // Startvärden m.m.
        rb = GetComponent<Rigidbody>();
        itsAlive = true;
        startPos = transform.position;
        startConstraints = rb.constraints;
    }

    // Update is called once per frame
    void Update()
    {
        if (itsAlive)
        {
            // Flytta hil framåt
            transform.position += transform.forward * speed * Time.deltaTime;

            // Styr Phil
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rb.AddForce(transform.up * jumpForce);
            } 
        }
        else
        {
            // Omstart av spelet
            if (Input.GetKeyDown(KeyCode.Return))
            {
                //itsAlive = true;
                //transform.position = startPos;
                //transform.localRotation = Quaternion.identity;
                //rb.constraints = startConstraints;

                // istället för ovan kan man istället göra detta:
                //
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                //
                // Det laddar in hela den aktiva scenen igen.
                // Det är inte alltid en bra idé, men här skulle det fungera bra

            }
        }

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        // När Phil kolliderar gör detta
        itsAlive = false;
        rb.constraints = RigidbodyConstraints.None;
    }

}
