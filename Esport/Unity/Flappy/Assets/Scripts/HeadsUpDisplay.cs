﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeadsUpDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textScore;
    [SerializeField] private TextMeshProUGUI textTime;
    [SerializeField] private PlayerProperties playerProperties;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textTime.text = "time " +  Time.timeSinceLevelLoad.ToString("000.0");
        textScore.text = "score " + playerProperties.Score.ToString("0000");
    }
}
