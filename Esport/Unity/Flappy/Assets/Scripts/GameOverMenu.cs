﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private string resetScene;

    public void Reset()
    {
        SceneManager.LoadScene(resetScene);
    }
}
