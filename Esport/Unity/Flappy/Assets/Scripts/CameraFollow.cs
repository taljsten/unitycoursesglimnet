﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Glöm inte att dra Flappybird till denna slot
    public Transform flappyTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3(x,y,z)

        transform.position = new Vector3(
            transform.position.x, 
            transform.position.y, 
            flappyTransform.position.z);
    }
}
