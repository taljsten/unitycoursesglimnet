﻿using System;

namespace HelloWorld02
{
    class Program
    { 
        static void Main(string[] args)
        {
            // Heltal deklareras som int
            int tal1 = 6;
            int tal2 = 4;
            int tal3 = tal1 + tal2; // addera
            int tal4 = tal1 / tal2; // ger heltalsdelen i en division
            int rest = tal1 % tal2; // modulus, ger resten

            // decimaltal eller rättare flyttal deklareras som float.
            float f1 = 5.5f;
            float f2 = 7.8f;
            // Uträkningar
            float sum = f1 + f2;
            float mul = f1 * f2;
            float div = f1 / f2;
            float sub = f1 - f2;

            // Skriv ut klabbet på skärmen i konsollfönstret
            Console.WriteLine("Hello world");
            Console.WriteLine("tal1 + tal2 är " + tal3);
            Console.WriteLine("Summan = " + sum);
            Console.WriteLine("Produkt = " + mul);
            Console.WriteLine("Division = " + div);
            Console.WriteLine("Subtraktion = " + sub);
            Console.WriteLine("Div av heltal = " + tal4);
            Console.WriteLine("rest av heltal = " + rest);

            // Bara en radmatning
            Console.WriteLine();

            // Vänta tills användaren skriver in en text (ett namn)
            Console.Write("Namn : ");
            string namn = Console.ReadLine();
            
            // Om namn är lika med Luke skriv Luke Skywalker på skärmen 
            // annars om namn är lika med Han skriv Han Solo på skärmen
            // annars Skriv Chewbacca på skärmen
            if (namn == "Luke") // Jämförelse == != > < >= <= 
            {
                Console.WriteLine("Luke Skywalker");
            }
            else if (namn == "Han") 
            {
                Console.WriteLine("Han Solo");
            }
            else 
            {
                Console.WriteLine("Chewbacca");
            }

            // Vänta på enter (och en text)
            Console.ReadLine();
        }
    }
}
