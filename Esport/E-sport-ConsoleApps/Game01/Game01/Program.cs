﻿using System;
using System.Threading;

namespace Game01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initiera saker
            bool isRunning = true;
            Random rng = new Random();

            // Starta spel-loopen
            while (isRunning == true)
            {             
                // Update, ett slumpat tecken
                char c = (char)rng.Next(25,254); 
                // Draw, rita ut på skärmen
                Console.Write(c);
                // "FPS" - timer
                Thread.Sleep(5);
                // Input, "typ"
                if (c == '9')
                {
                    isRunning = false;
                }
            }
        }
    }
}
