﻿using System;


namespace Adventure
{
    class Program
    {
        static void Main(string[] args)
        {
            // Start section
            bool isRunning = true;

            int posX = 0;
            int posY = 0;
            string[,] rooms = new string[2, 2];
            rooms[0, 0] = "** Forest **\n" +
                "You find yourself alone in a dark forest. A small path leads to the north and a light shines there.\n" +
                "To the east you can hear a dog barking.\n";

            rooms[0, 1] = "** A candle in the wind ** \n" +
                "A candle burns. At the ground dandy lines are growing.\n" +
                "To the south there's darkness. The dandy lines continue growing in a path to the east.";

            rooms[1, 0] = "** Wag the tail **\n" +
                "There's a chained dog barking. He doesn't look friendly.\n" +
                "To the west you can see high trees and darkness.\n" +
                "To the north is a yellow glow.\n";

            rooms[1, 1] = "** Dandy **\n" +
                "The Dandy Lines are wast in numbers.\n" +
                "You have to swim to get through.\n" +
                "You can swim to the west and south.\n";

            Console.Clear();
            Console.WriteLine("");
            Console.WriteLine("Welcome to some sort of Adventure");
            Console.WriteLine("Your in a forest. ");
            Console.WriteLine("The night is heavy and foggy.");
            Console.WriteLine();
            Console.WriteLine("Commands for navigating is n(orth), s(outh), w(est), e(ast). Use q(uit) to quit");
            Console.WriteLine();
            Console.WriteLine("Press Enter to start playing.");
            Console.ReadLine();

            while (isRunning)
            {
                // Draw
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine(rooms[posX, posY]);
                Console.WriteLine();
                Console.Write("Where do you want to go: ");

                // Input
                string answer = Console.ReadLine();
                if (string.IsNullOrEmpty(answer))
                    answer = "---";

                // Update
                if (answer.ToUpper()[0] == 'N')
                {
                    posY++;
                }
                if (answer.ToUpper()[0] == 'S')
                {
                    posY--;
                }
                if (answer.ToUpper()[0] == 'W')
                {
                    posX--;
                }
                if (answer.ToUpper()[0] == 'E')
                {
                    posX++;
                }
                if (answer.ToUpper()[0] == 'Q')
                {
                    isRunning = false;
                }

                if (posX < 0)
                {
                    posX = 0;
                    Console.WriteLine();
                    Console.WriteLine("There's nothing to the west");
                    Console.WriteLine("Press enter to continue your quest.");
                    Console.ReadLine();
                }

                if (posX > 1)
                {
                    posX = 1;
                    Console.WriteLine();
                    Console.WriteLine("There's nothing to the east");
                    Console.WriteLine("Press enter to continue your quest.");
                    Console.ReadLine();
                }

                if (posY < 0)
                {
                    posY = 0;
                    Console.WriteLine();
                    Console.WriteLine("There's nothing to the south");
                    Console.WriteLine("Press enter to continue your quest.");
                    Console.ReadLine();
                }

                if (posY > 1)
                {
                    posY = 1;
                    Console.WriteLine();
                    Console.WriteLine("There's nothing to the north");
                    Console.WriteLine("Press enter to continue your quest.");
                    Console.ReadLine();
                }
            }


            // End section
            Console.Clear();
            Console.WriteLine("Thank you for playing, welcome back");            
            Console.ReadLine();
        }
    }
}
