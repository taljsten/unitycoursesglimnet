﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsolePossibilities
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rng = new Random();
            bool isRunning = true;

            Console.WindowHeight = 40;
            Console.WindowWidth = 85;
            Console.BufferHeight = 40;
            Console.BufferWidth = 85;


            Console.WriteLine("                                   ,/#%&&%%#/,                                  ");
            Console.WriteLine("                      /&&&&&&&&%(*,..........,,*(&&&&&&&&%,                     ");
            Console.WriteLine("                      /&&&&&&&&%(*,..........,,*(&&&&&&&&%,                     ");
            Console.WriteLine("                %&&&&#,....,..............................,%&&&&/               ");
            Console.WriteLine("           ,&&&%...*%&&&&&.........%&,......&&.........&&&&&%,..,&&&&           ");


            Console.ReadLine();


            //List<string> rows = new List<string>();
            //try
            //{
            //    using (StreamReader sr = new StreamReader("ascii-art.txt"))
            //    {
            //        while (!sr.EndOfStream)
            //        {
            //            rows.Add(sr.ReadLine());
            //        }
            //        sr.Close();
            //    }
            //}
            //catch (Exception e)
            //{

            //    throw e;
            //}

            //foreach (string row in rows)
            //{
            //    Console.WriteLine(row);
            //}
            //Console.ReadLine();


            while (isRunning)
            {
                //Console.BackgroundColor = ConsoleColor.Cyan;
                Console.SetCursorPosition(5, 3);
                Console.ForegroundColor = (ConsoleColor)rng.Next(2, 12);
                Console.Write("C");
                Console.ForegroundColor = (ConsoleColor)rng.Next(2, 12);
                Console.Write("O");
                Console.ForegroundColor = (ConsoleColor)rng.Next(2, 12);
                Console.Write("L");
                Console.ForegroundColor = (ConsoleColor)rng.Next(2, 12);
                Console.Write("O");
                Console.ForegroundColor = (ConsoleColor)rng.Next(2, 12);
                Console.Write("R");

               
                Thread.Sleep(500);
            }
        }
    }
}
