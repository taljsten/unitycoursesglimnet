﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoveX
{
    class Program
    {
        static void Main(string[] args)
        {
            // Init
            bool isRunning = true;
            ConsoleKeyInfo keyInfo;
            int posX = 0;
            int posY = 0;

            int oldPosX = 1;
            int oldPosY = 1;
            
            Console.CursorVisible = false;
            
            while (isRunning)
            {
                // Input
                if (Console.KeyAvailable)
                {
                    keyInfo = Console.ReadKey(true);
                    switch (keyInfo.Key)
                    {
                        case ConsoleKey.Escape:
                            isRunning = false;
                            break;
                        case ConsoleKey.W:
                        case ConsoleKey.UpArrow:
                            posY--;
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.DownArrow:
                            posY++;
                            break;
                        case ConsoleKey.A:
                        case ConsoleKey.LeftArrow:
                            posX--;
                            break;
                        case ConsoleKey.D:
                        case ConsoleKey.RightArrow:
                            posX++;
                            break;
                        default:
                            break;
                    }                   
                }

                // Update
                if (posX < 0)
                {
                    posX = 0;
                }
                if (posX > 20)
                {
                    posX = 20;
                }
                if (posY < 0)
                {
                    posY = 0;
                }
                if (posY > 10)
                {
                    posY = 10;
                }

                // Draw
                //Console.Clear();

                if (oldPosX != posX || oldPosY != posY)
                {
                    Console.SetCursorPosition(posX, posY);
                    Console.Write("x");
                    Console.SetCursorPosition(oldPosX, oldPosY);
                    Console.Write(" ");
                    oldPosX = posX;
                    oldPosY = posY;
                }

            }
        }


    }
}
