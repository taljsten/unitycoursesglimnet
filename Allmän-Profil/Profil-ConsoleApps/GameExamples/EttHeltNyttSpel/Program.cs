﻿using System;
using System.Threading;

// Ert namespace kan vara annorlunda
namespace EttHeltNyttSpel
{
    class Program
    {
        // Entry point för programmet
        static void Main(string[] args)
        {
            // Inits, variables and other stuff
            Console.WindowHeight = 40; 
            Console.WindowWidth = 80; 
            Console.BufferHeight = 40; 
            Console.BufferWidth = 80;
            Console.CursorVisible = false;
            bool isRunning = true;
            int x = 5;
            int y = 6;
            int dirX = 0;
            int dirY = 1;

            // StartScreen
            Console.WriteLine("Welcome to the new game");
            Console.WriteLine("Press Enter to play");
            Console.ReadLine();
            Console.Clear();

            // Game loop
            while (isRunning)
            {
                // Input
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                    switch (keyInfo.Key)
                    {                        
                        case ConsoleKey.A:                        
                            dirX = -1;
                            dirY = 0;
                            break;                        
                        case ConsoleKey.D:
                            dirX = 1;
                            dirY = 0;
                            break;                        
                        case ConsoleKey.S:
                            dirX = 0;
                            dirY = 1;
                            break;                        
                        case ConsoleKey.W:
                            dirX = 0;
                            dirY = -1;
                            break;
                        case ConsoleKey.Escape:
                            isRunning = false;
                            break;
                        default:
                            break;
                    }
                }

                // Update 
                x = x + dirX;
                y = y + dirY;

                if (x == -1)
                {
                    x = Console.BufferWidth-1; // eller 79
                }

                if (y == -1)
                {
                    y = Console.BufferHeight-1; // eller 39
                }

                if (x == Console.BufferWidth) // eller 80
                {
                    x = 0;
                }

                if (y == Console.BufferHeight) // eller 40
                {
                    y = 0;
                }

                // Draw
                Console.SetCursorPosition(x, y);
                Console.Write("o");

                Thread.Sleep(50); 
            }

            // Game over screen
        }

            
    }
}
