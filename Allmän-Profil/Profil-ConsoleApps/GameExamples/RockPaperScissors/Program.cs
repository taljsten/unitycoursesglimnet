﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variables that needs to be accessed through all program
            string[] choices = new string[] { "Rock", "Paper", "Scissors" };
            Random rng = new Random();
            bool isRunning = true;
            int playerScore = 0;    
            int computerScore = 0;  

            // Start screen
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Welcome to a game of");
            Console.WriteLine("Rock - Paper - Scissors");
            Console.WriteLine("Press ENTER to start playing");
            Console.ReadLine();

            // Game loop
            while (isRunning)
            {

                // Draw menu
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Clear();

                Console.WriteLine();
                Console.WriteLine("Score (You - Computer): " + playerScore + " - " + computerScore);
                Console.WriteLine();

                Console.WriteLine("Choose:");
                Console.WriteLine("A. " + choices[0] );
                Console.WriteLine("B. " + choices[1] );
                Console.WriteLine("C. " + choices[2] );
                Console.WriteLine("Q. Exit Game");
                Console.WriteLine();


                // Take and handle input
                int playerChoice = -1; // important to set this to -1, checking below if we get an valid input

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.A:                        
                        playerChoice = 0;
                        break;
                    case ConsoleKey.B:
                        playerChoice = 1;
                        break;
                    case ConsoleKey.C:
                        playerChoice = 2;
                        break;
                    case ConsoleKey.Q:
                        isRunning = false;
                        break;
                    default:                        
                        break;
                }

                // Feedback to player if valid input
                if (playerChoice != -1)
                {
                    Console.WriteLine("Your choice: " + choices[playerChoice]);
                    //Console.Write("  Waiting for computer");
                    for (int i = 0; i < 3; i++)
                    {
                        Console.Write(" "+ (i+1));
                        Thread.Sleep(500);
                    }
                    Console.WriteLine();
                    int computerChoice = rng.Next(0, choices.Length);
                    Console.WriteLine("Computer's choice: " + choices[computerChoice]);
                    
                    // Checking all possibilties 
                    if (playerChoice == computerChoice) // both chose the same "tool"
                    {
                        Console.WriteLine("It's a Draw");
                    }
                    else if (playerChoice == 0 && computerChoice == 2) // player chose rock, computer chose scissor
                    {
                        Console.WriteLine("You won!");
                        playerScore++;
                    }
                    else if (playerChoice == 1 && computerChoice == 0) // player chose paper, computer chose rock
                    {
                        Console.WriteLine("You won!");
                        playerScore++;
                    }
                    else if (playerChoice == 2 && computerChoice == 1) // player chose scissor, computer chose paper
                    {
                        Console.WriteLine("You won!");
                        playerScore++;
                    }
                    else // In the three remaining computer wins, no need to write if statements for them
                    {
                        Console.WriteLine("Computer won!");
                        computerScore++;
                    }
                   

                    // Just waiting, and after everything restarts
                    Console.WriteLine();

                    for (int i = 0; i < 7; i++)
                    {
                        Console.Write(".");
                        Thread.Sleep(500);
                    }
                    Console.WriteLine();

                }
            }

            // End screen
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Thanks for playing");
            if (playerScore > computerScore)
            {
                Console.WriteLine("  YOU WON! ");
            }
            else if (playerScore == computerScore)
            {
                Console.WriteLine("  IT'S A DRAW! ");
            }
            else
            {
                Console.WriteLine("  COMPUTER WON! ");
            }
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }
    }
}
