﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GumBallsInAJar
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variables
            Random rnd = new Random();
            bool isRunning = true;
            int noOfGuesses = 0;
            int noOfGumballs = rnd.Next(0, 200);

            // Startsida
            Console.Clear();
            Console.WriteLine("***   Welcome   ***");
            Console.WriteLine(" Gum-Balls in A Jar");
            Console.WriteLine("*** Press a key ***");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine();

            // Game loop
            while (isRunning)
            {
                // Draw, Update, Input
                Console.WriteLine();
                Console.Write("How many gum balls are in my jar? ");
                string guessText = Console.ReadLine();
                int guess = int.Parse(guessText);
                if (guess < noOfGumballs)
                {
                    Console.WriteLine("Your guess is to low.");
                }

                if (guess > noOfGumballs)
                {
                    Console.WriteLine("Your guess is to high.");
                }

                if (guess == noOfGumballs)
                {
                    Console.WriteLine("Your guess is correct. Press a key");
                    Console.ReadKey();
                    isRunning = false;
                }
                noOfGuesses++;
            }

            // Game over
            Console.Clear();
            Console.WriteLine("Game over.");            
            Console.WriteLine("You needed " + noOfGuesses + " guesses");
            Console.WriteLine("Press a key to exit game");
            Console.ReadKey();

        }
    }
}
