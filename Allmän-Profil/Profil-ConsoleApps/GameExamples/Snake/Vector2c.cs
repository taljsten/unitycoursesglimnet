﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public struct Vector2c
    {
        public int x;
        public int y;

        public Vector2c(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
