﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Snake
{
    public class Game:IDisposable
    {

        private Vector2c pos;
        private Vector2c dir;
        private int delay;

        public Game(int delay)
        {
            this.delay = delay;
            pos = new Vector2c(10, 10);
            dir = new Vector2c(0, 1);
        }

        public void Run()
        {
            
            bool isrunning = true;
            Console.CursorVisible = false;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Welcome to Snake");
            Console.WriteLine("Press enter to start playing");
            Console.ReadLine();
            Console.Clear();

            while (isrunning)
            {

                // Input
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo keyinfo = Console.ReadKey(true);

                    switch (keyinfo.Key)
                    {
                        case ConsoleKey.W:
                            dir = new Vector2c(0, -1);
                            break;
                        case ConsoleKey.A:
                            dir = new Vector2c(-1, 0);
                            break;
                        case ConsoleKey.S:
                            dir = new Vector2c(0, 1);
                            break;
                        case ConsoleKey.D:
                            dir = new Vector2c(1, 0);
                            break;
                        case ConsoleKey.Escape:
                            isrunning = false;
                            break;
                        default:
                            
                            break;
                    }
                }

                // Update
                pos.x = pos.x + dir.x;
                pos.y = pos.y + dir.y;

                if (pos.x > 40)
                    pos.x = 0;
                if (pos.x < 0)
                    pos.x = 40;
                if (pos.y > 25)
                    pos.y = 0;
                if (pos.y < 0)
                    pos.y = 25;


                // Draw
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.SetCursorPosition(pos.x, pos.y);
                Console.Write("o");

                Thread.Sleep(delay);
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Clear();
            Console.WriteLine("Thanks for playing");
            Console.WriteLine("Press enter to exit game");
            Console.ReadLine();

        }

        public void Dispose()
        {
            //
        }
    }
}
