﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Catch
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            int playerPosX = 0; ;
            int playerPosY = 0;
            int frameDelay = 20;
            Console.CursorVisible = false;
            ConsoleKeyInfo keyInfo;

            while (isRunning)
            {
                
                // Input
                if (Console.KeyAvailable)
                {
                    keyInfo = Console.ReadKey(true);
                    switch (keyInfo.Key)
                    {
                        case ConsoleKey.Escape:
                            isRunning = false;
                            break;
                        case ConsoleKey.W:
                        case ConsoleKey.UpArrow:
                            playerPosY--;
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.DownArrow:
                            playerPosY++;
                            break;
                        case ConsoleKey.A:
                        case ConsoleKey.LeftArrow:
                            playerPosX--;
                            break;
                        case ConsoleKey.D:
                        case ConsoleKey.RightArrow:
                            playerPosX++;
                            break;
                        default:
                            break;
                    }
                }


                //playerPosX++;
                //playerPosY++;

                //Console.Clear();
                Console.SetCursorPosition(playerPosX, playerPosY);
                Console.Write("x");


                Thread.Sleep(frameDelay);
                
            }
        }
    }
}
