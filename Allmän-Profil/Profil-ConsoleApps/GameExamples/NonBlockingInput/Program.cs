﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonBlockingInput
{
    class Program
    {
        // Fundera gärna på vad nedanstående kod gör rad för rad
        static void Main(string[] args)
        {
            bool isRunning = true;
            Console.CursorVisible = false;

            while (isRunning)
            {
                string message = "";

                // Non Blocking input
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                    switch (keyInfo.Key)                   
                    {                        
                        case ConsoleKey.Escape:
                            isRunning = false;
                            break;                        
                        case ConsoleKey.LeftArrow:
                            message = "Going left";
                            break;
                        case ConsoleKey.UpArrow:
                            message = "Going up";
                            break;
                        case ConsoleKey.RightArrow:
                            message = "Going right";
                            break;
                        case ConsoleKey.DownArrow:
                            message = "Going down";
                            break;                        
                        default:                            
                            break;
                    }
                }

                // Update

                // Draw
                if (message != "")
                {
                    Console.SetCursorPosition(5, 5);
                    Console.WriteLine(message + "                   "); 
                }
            }
        }
    }
}
