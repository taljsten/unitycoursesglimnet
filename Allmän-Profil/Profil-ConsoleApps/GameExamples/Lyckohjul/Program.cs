﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lyckohjul
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                Console.Write("Make a bet on 1-10: "); string sBet = Console.ReadLine();  
                //int nr = rnd.Next(1, 11);                
                //string sWin = nr.ToString();                
                int stopSpin = rnd.Next(20, 30);                
                string sWin=Spin(stopSpin);
                Console.WriteLine();
                Console.WriteLine("You betted on " + sBet + " and winning number is " + sWin);
                if (sWin == sBet)                
                {                    
                    Console.WriteLine("You won!");                
                }               
                else                
                {                   
                    Console.WriteLine("You lost!");             
                }            
            }            
            Console.ReadLine();
        }

        public static string Spin(int stop)
        {
            int counter = 0;
            int nr = 0;
            for (int i = 1; i <= 10; i++)
            {
                nr = i; 
                Console.Clear(); 
                Console.Write(i); 
                Thread.Sleep(500); 

                if (i == 10) 
                { 
                    i = 0; 
                }

                counter++; 
                if (counter == stop) 
                { 
                    break;
                }
            }
            return nr.ToString();
        }
    }
}
