﻿using System;

namespace HelloWorld04
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 7;
            int b = 10;

            int sum = a + b;
            int mul = a * b;
            int sub = a - b;            

            Console.WriteLine("Hello World");
            Console.WriteLine("Summa: " + sum);
            Console.WriteLine("Produkt: " + mul);
            Console.WriteLine("Subtraktion: " + sub);
            
            Console.ReadLine();
        }
    }
}
