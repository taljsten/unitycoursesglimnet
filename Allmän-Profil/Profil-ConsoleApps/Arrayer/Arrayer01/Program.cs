﻿using System;

namespace Arrayer01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Skapa en array och slumpa värden i den
            Random rng = new Random();
            int[] slumpadeTal = new int[10000];
            for (int i = 0; i < slumpadeTal.Length; i++)
            {
                slumpadeTal[i] = rng.Next(0, 1000);
            }

            // "Vanlig" for loop = starta på index 0 och räkna fram till sista index i arrayen
            for (int i = 0; i < slumpadeTal.Length; i++)
            {
                Console.Write(slumpadeTal[i] + " ");
            }

            // foreach = för alla tal i slumpade tal
            foreach (int tal in slumpadeTal)
            {
                Console.Write(tal + " ");
            }

            

        }
    }
}
