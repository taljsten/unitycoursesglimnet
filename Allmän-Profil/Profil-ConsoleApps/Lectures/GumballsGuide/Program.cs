﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GumballsGuide
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rng = new Random();
            bool isRunning = true;
            int noOfGumballs = rng.Next(50, 100);

            Console.WriteLine("Welcome to gumballs");
            Console.WriteLine("Press Enter to Start");
            Console.ReadLine();
            Console.Clear();


            while(isRunning)
            {
                Console.Clear();
                Console.Write("Guess number of gumballs: ");
                string guessString = Console.ReadLine();
                int guess = int.Parse(guessString);

                if (guess > noOfGumballs)
                {
                    Console.WriteLine("Your guess is to high");
                    Thread.Sleep(1000);
                }




            }

            Console.Clear();
            Console.WriteLine("Game Over");
            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();
            
        }
    }
}
