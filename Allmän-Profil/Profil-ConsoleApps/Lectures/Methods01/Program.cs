﻿using System;

namespace Methods01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Summa(7, 10));
            Console.WriteLine(Summa(5, 4));
            Console.WriteLine(Summa(56, 4));
            Console.WriteLine(Summa(5, 8));
            Console.WriteLine(Summa(1, 4));
            Console.WriteLine(Summa(5, 7));

            int a = Summa(10, 9) * Summa(12, 2);  // int * int
            Console.WriteLine(a);
            Console.WriteLine(Matte(5.6f));
            PrintName("Boba", "Fett");
        }

        private static int Summa(int tal1, int tal2)
        {
            Console.WriteLine("Summerar " + tal1 + " och " + tal2);
            return tal1 + tal2;
        }

        private static float Matte(float x)
        {
            return x * x + x + 5;
        }

        private static void PrintName(string name, string lastName)
        {
            Console.WriteLine("Hej! Välkommen " + name + " " + lastName);
        }
    }
}
