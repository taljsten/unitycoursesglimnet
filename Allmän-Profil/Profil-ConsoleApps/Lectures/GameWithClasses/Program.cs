﻿using System;
using System.Threading;

namespace GameWithClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inställningar
            bool isRunning = true;
            Console.CursorVisible = false;

            // Spelobjekt
            Snake orm = new Snake(2, 5);

            // Spel-loop
            while (isRunning)
            {
                // Update
                orm.Update();
                Thread.Sleep(100);

                // Draw
                orm.Draw();
            }

        }
    }

   
}
