﻿using System;

namespace GameWithClasses
{
    class Snake
    {
        // klass data
        private int x;
        private int y;

        // konstruktor
        public Snake(int startX, int startY)
        {
            x = startX;
            y = startY;
        }

        // Uppdatera snake
        public void Update()
        {
            x++;
        }

        // Rita ut snake
        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.Write("x");
        }

    }
}
