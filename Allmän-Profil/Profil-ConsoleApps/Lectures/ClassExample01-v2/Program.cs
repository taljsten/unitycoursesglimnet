﻿using System;

namespace ClassExample01
{
    class Program
    {
        static void Main(string[] args)
        {
            Hund labbe = new Hund("Labrador Retreiver", "Beige", 35);
            Hund chihuahua = new Hund("Chihuahua", "Svart", 1);
            Hund newFoundland = new Hund("New Foundland", "Svart", 35);

            labbe.Print();
            chihuahua.Print();
            newFoundland.Print();
        }
    }

    class Hund
    {
        string ras;
        string färg;
        int mankhöjd;

        public Hund(string ras, string färg, int mankhöjd)
        {
            this.ras = ras;
            this.färg = färg;
            this.mankhöjd = mankhöjd;
        }

        public void Print()
        {
            Console.WriteLine(ras);
            Console.WriteLine(färg);
            Console.WriteLine(mankhöjd);
        }
    }
}