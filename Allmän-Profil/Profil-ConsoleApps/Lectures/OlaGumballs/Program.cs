﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace Gum_balls
{ 
    class Program 
    { 
        static void Main(string[] args) 
        { 
            bool isRunning = true; 
            Random rung = new Random(); 
            int no0fGumballs = rung.Next(50, 100); 
            Console.WriteLine("Welcome to gum balls"); 
            Console.WriteLine("press Enter to play"); 
            Console.ReadLine(); 
            Console.Clear();               
            // Spel-loop             
            while (isRunning)             
            {                
                //Console.Clear();                 
                Console.Write("Guss number of gumballs: ");                 
                string guessString = Console.ReadLine();                
                int guess = int.Parse(guessString);                
                if (guess > no0fGumballs)             
                {                    
                    Console.WriteLine("your guess is to hign");   
                    Thread.Sleep(1000);                     
                }                 
                if (guess < no0fGumballs)    
                {               
                    Console.WriteLine("your guess is to low");    
                    Thread.Sleep(1000);             
                }             
                if (guess == no0fGumballs)              
                {                   
                    Console.WriteLine("your guess is right");       
                    Thread.Sleep(1000);                  
                    isRunning = false;              
                }            
            }         
            // Console.Clear();       
            Console.WriteLine("game over");    
            Console.WriteLine("press Enter to end");   
            Console.ReadLine();                   
        }     
    } 
}                                                                 