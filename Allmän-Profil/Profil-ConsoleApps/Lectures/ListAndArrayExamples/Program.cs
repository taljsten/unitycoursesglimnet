﻿using System;
using System.Collections.Generic; // Listor
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndArrayExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            // Statisk
            int[] fleraTal = new int[] { 0, 1, 2, 3, 4, 5 }; // default värde 0

            fleraTal[2] = 7;
            fleraTal[5] = 0;

            for (int i = 0; i < fleraTal.Length; i++)
            {
                Console.WriteLine(fleraTal[i]);
            }

            Console.WriteLine();

            List<int> fleraTal2 = new List<int>();
            fleraTal2.Add(45);
            fleraTal2.Add(46);
            fleraTal2.Add(48);
            fleraTal2.Add(48);
            fleraTal2.Add(48);

            for (int i = 0; i < fleraTal2.Count; i++)
            {
                Console.WriteLine(fleraTal2[i]);
            }

            fleraTal2.Remove(48);
            Console.WriteLine();

            for (int i = 0; i < fleraTal2.Count; i++)
            {
                Console.WriteLine(fleraTal2[i]);
            }

            fleraTal2.RemoveAt(2);
            Console.WriteLine();

            for (int i = 0; i < fleraTal2.Count; i++)
            {
                Console.WriteLine(fleraTal2[i]);
            }

        }
    }
}
