﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassExample01
{
    class Program
    {
        static void Main(string[] args)
        {
            Car polo = new Car("VW Polo", "Red", 150);
            Car mini = new Car("Mini", "Blue", 170);
            Car beetle = new Car("VW Beetle", "Black", 255);

            polo.Print();
            mini.Print();
            beetle.Print();
        }
    }
}
