﻿using System;

namespace ClassExample01
{
    class Car
    {
        string brand;
        string color;
        int horsePower;

        public Car(string brand, string color, int horsePower)
        {
            this.brand = brand;
            this.color = color;
            this.horsePower = horsePower;
        }

        public void Print()
        {
            Console.WriteLine(brand);
            Console.WriteLine(color);
            Console.WriteLine(horsePower);
        }
    }
}
