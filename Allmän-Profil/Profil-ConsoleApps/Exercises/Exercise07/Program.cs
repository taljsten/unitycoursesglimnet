﻿using System;

namespace Exercise07
{
    /// <summary>
    /// Detta är ett sätt att göra en startsida på.
    /// Skall man skifta mellan mer än två "sidor" är 
    /// det bättre att använda en enum och en switch.
    /// --
    /// Övningen är frivillig och skall ses som hjälp mer än övning, 
    /// även om jag rekommenderar att ni testar koden för att se
    /// hur det fungerar.
    /// ***
    /// David Täljsten, 2020-09-21 
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            bool isPlaying = false;               
            Console.CursorVisible = false;

            while (isRunning)
            {
                if (isPlaying == false)
                {
                    Console.SetCursorPosition(5, 10);
                    Console.Write("START PAGE");
                    Console.SetCursorPosition(5, 12);
                    Console.Write("Menu: ");
                    Console.SetCursorPosition(6, 13);
                    Console.Write("F1. Spela");
                    Console.SetCursorPosition(6, 14);
                    Console.Write("F2. Avsluta ");

                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.F1)
                    {
                        isPlaying = true;
                        Console.Clear();
                    }
                    else if (keyInfo.Key == ConsoleKey.F2)
                    {
                        isRunning = false;
                    }
                }
                else
                {
                    Console.SetCursorPosition(10, 10);
                    Console.Write("Game running ");
                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.Escape)
                    {
                        isPlaying = false;
                        Console.Clear();
                    }
                }
            }
        }
    }
}
