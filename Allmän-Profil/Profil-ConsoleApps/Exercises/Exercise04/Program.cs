﻿using System;

namespace Exercise04
{
    class Program
    {
        static void Main(string[] args)
        {                        
            Console.Write("Skriv ett heltal : ");
            string inputText = Console.ReadLine();

            if (int.TryParse(inputText, out int input))
            {
                switch(input)
                {
                    case 0:
                        Console.WriteLine("Du skrev en nolla");
                        break;
                    case 1:
                        Console.WriteLine("Du skrev en etta");
                        break;
                    case 2:
                        Console.WriteLine("Du skrev en tvåa");
                        break;                    
                    default:
                        Console.WriteLine("Du skrev ett icke hanterat tal");
                        break;
                }
            }
            else
            {
                Console.WriteLine("ERROR: Felaktig inmatning");
            }

            Console.ReadLine();
        }
    }
}
