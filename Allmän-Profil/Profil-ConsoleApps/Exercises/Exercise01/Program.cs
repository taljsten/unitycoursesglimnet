﻿using System;

namespace Exercise01
{
    class Program
    {
        static void Main(string[] args)
        {
            int tal1 = 12;
            int tal2 = 7;
            int summa = tal1 + tal2;
            int differens = tal1 - tal2;
            int produkt = tal1 * tal2;
            int kvot = tal1 / tal2;
            int rest = tal1 % tal2;            
            
            Console.WriteLine("Hello World");
            Console.WriteLine("Mitt första tal = " + tal1);
            Console.WriteLine("Mitt andra tal = " + tal2);
            Console.WriteLine("Summa = " + summa);
            Console.WriteLine("Differens = " + differens);
            Console.WriteLine("Produkt = " + produkt);
            Console.WriteLine("Kvot = " + kvot);
            Console.WriteLine("Rest = " + rest);           

            Console.ReadLine();
        }
    }
}
