﻿using System;

namespace Exercise02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("** MITT SUPER DUPER PROGRAM **");
            Console.Write("Skriv ditt förnamn: ");
            string name = Console.ReadLine();
            Console.WriteLine("Välkommen till Matrix " + name);
            
            Console.WriteLine();
            string nyttNamn = "Luke " + name.Trim() + " Skywalker";
            Console.WriteLine("Välkommen till Tatooine " + nyttNamn);
            
            Console.Write("\nSkriv ett tal: ");
            string talsträng = Console.ReadLine();
            int tal = int.Parse(talsträng);
            tal = tal * tal;
            Console.WriteLine("Ditt tal upphöjt till 2 blir " + tal);

            // Om du kör programmet med CTRL + F5 behövs inte ReadLine() här
            Console.ReadLine();
        }
    }
}
