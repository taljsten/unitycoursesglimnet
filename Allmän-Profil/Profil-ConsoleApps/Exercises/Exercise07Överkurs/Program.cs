﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise07Överkurs
{
    public enum GameState
    {
        MainMenu,
        Playing,
        GameOver
    }

    /// <summary>
    /// 
    /// OBS! Detta är frivilligt och överkurs just nu.
    /// 
    /// Så här skulle en enkel meny se ut
    /// med enum / switch istället för if else (som i övning 07).
    /// David Täljsten, 2020-09-21
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            GameState gameState = GameState.MainMenu;
            Console.CursorVisible = false;
            ConsoleKeyInfo keyInfo;

            while (isRunning)
            {
                switch (gameState)
                {
                    case GameState.MainMenu:
                        Console.SetCursorPosition(5, 10);
                        Console.Write("START PAGE");
                        Console.SetCursorPosition(5, 12);
                        Console.Write("Menu: ");
                        Console.SetCursorPosition(6, 13);
                        Console.Write("F1. Spela");
                        Console.SetCursorPosition(6, 14);
                        Console.Write("F2. Avsluta ");
                        keyInfo = Console.ReadKey();
                        if (keyInfo.Key == ConsoleKey.F1)
                        {
                            gameState=GameState.Playing;
                            Console.Clear();
                        }
                        else if (keyInfo.Key == ConsoleKey.F2)
                        {
                            isRunning = false;
                        }
                        break;
                    case GameState.Playing:
                        Console.SetCursorPosition(10, 10);
                        Console.Write("Game running ");
                        keyInfo = Console.ReadKey();
                        if (keyInfo.Key == ConsoleKey.Escape)
                        {
                            gameState = GameState.GameOver;
                            Console.Clear();
                        }
                        break;
                    case GameState.GameOver:
                        Console.SetCursorPosition(10, 10);
                        Console.Write("GAME OVER");
                        keyInfo = Console.ReadKey();
                        if (keyInfo.Key == ConsoleKey.Escape)
                        {
                            gameState = GameState.MainMenu;
                            Console.Clear();
                        }
                        break;                   
                    default:
                        break;
                }

                
              
            }
        }
    }
}
