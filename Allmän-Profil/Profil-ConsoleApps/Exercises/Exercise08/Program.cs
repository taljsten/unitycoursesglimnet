﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise08
{
    /// <summary>
    /// Frivillig övning, men hjälper till för att förstå
    /// en dubbel array
    /// 
    /// https://www.ascii-code.com/
    /// 
    /// Denna kod skriver ut en grid med slumpade bokstäver (A-Z).
    /// I de första två for looparna lägger vi in slumpade bokstäver i arrayen
    /// och i de sista två for looparna skriver vi ut dem på skärmen.
    /// 
    /// Slumpgeneratorn kommer aldrig att slumpa 91 nedan eftersom det är så den fungerar.
    /// rng.Next(65, 91) innebär lika med eller större än 65 och mindre än 91.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            char[,] characters = new char[10, 10];
            Random rng = new Random();

            for (int i = 0; i < characters.GetLength(0); i++)
            {
                for (int j = 0; j < characters.GetLength(1); j++)
                {
                    characters[i, j] = (char)rng.Next(65, 91); // Ascii kod för A är 65 och 90 för Z. 
                }

            }

            for (int i = 0; i < characters.GetLength(0); i++)
            {
                for (int j = 0; j < characters.GetLength(1); j++)
                {
                    Console.SetCursorPosition(i, j);
                    Console.WriteLine(characters[i,j]); 
                }

            }
        }
    }
}
