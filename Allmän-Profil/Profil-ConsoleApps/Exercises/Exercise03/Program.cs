﻿using System;

namespace Exercise03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input : ");
            string input = Console.ReadLine();
            input = input.ToLower();
            if (input == "luke")
            {
                Console.WriteLine("Luke Skywalker är son till Anakin Skywalker.");                
            }
            else if (input == "han")
            {
                Console.WriteLine("När Han Solo dyker upp första gången i ");
                Console.WriteLine("Stjärnornas krig är han en frilansande rymdkapten");
            }
            else if (input == "chewbacca")
            {                
                Console.WriteLine("Chewbacca är en långhårig varelse");
                Console.WriteLine("som härstammar från planeten Kashyyyk.");
            }
            else
            {
                Console.WriteLine("Den första i filmserien Stjärnornas krig,");                
                Console.WriteLine("hade premiär 25 maj 1977.");                
            }
            Console.ReadLine();
        }
    }
}
