﻿using System;
using System.Threading;

namespace Game01
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            Random rng = new Random();
           
            // spel-loop
            while (isRunning)
            {
                // Update
                char c = (char)rng.Next(25, 254);   // Tecken i ASCII tabellen               
                Thread.Sleep(10);                   // Högre siffra = långsammare program

                // Draw
                Console.Write(c);                   // Skriv ut ett tecken

                // "Input" system
                if (c=='5')             // Om det slumpade tecknet är 5 -> avsluta programmet
                {
                    isRunning = false;
                }
            }
        }
    }
}
