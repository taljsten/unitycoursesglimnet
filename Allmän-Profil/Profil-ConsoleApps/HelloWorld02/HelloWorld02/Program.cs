﻿using System;

namespace HelloWorld02
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int a = 5;
            int b = 7;
            int sum = a + b;

            float f1 = 5.5f;
            float f2 = 4.3f;
            float produkt = f1 * f2;            
            
            Console.WriteLine("Hej");
            Console.WriteLine("Summan av a och b är " + sum);
            Console.WriteLine("Produkten av f1 och f2 är " + produkt);

            Console.Write("Skriv in något och tryck på enter: ");
            string svar = Console.ReadLine();
            Console.WriteLine("Du skrev:  " + svar);

            Console.ReadKey();
        }
    }
}
