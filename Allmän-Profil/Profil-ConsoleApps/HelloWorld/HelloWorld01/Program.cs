﻿using System;

namespace HelloWorld01
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 2;
            int sum = a + b;
            int dif = a - b;
            int mul = a * b;
            int div = a / b;
            int res = a % b;

            float f1 = 7.5f;
            float f2 = 2.1f;
            float fSum = f1 + f2;

            string namn = "Nils";
            string efterNamn = "Plutt";
            string helaNamnet = namn + " " + efterNamn;

            bool ärSant = false;

            Console.WriteLine("Hello " + helaNamnet);
            Console.WriteLine("a + b = " + sum);
            Console.WriteLine("a - b = " + dif);
            Console.WriteLine("a * b = " + mul);
            Console.WriteLine("a / b = " + div);
            Console.WriteLine("a % b = " + res + " (rest)");
            Console.WriteLine();
            Console.WriteLine("f1 + f2 = " + fSum);

            if (ärSant)
            {
                Console.WriteLine("Jo, det är sant");
            }
            else
            {
                Console.WriteLine("Nej, det stämmer inte");
            }

            Console.ReadLine();
        }
    }
}
