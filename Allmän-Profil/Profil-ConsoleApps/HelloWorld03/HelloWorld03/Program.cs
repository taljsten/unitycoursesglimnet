﻿using System;

namespace HelloWorld03
{
    class Program
    {
        static void Main(string[] args)
        {
            // Här sker vår input
            Console.Write("Skriv något :");
            string val = Console.ReadLine();

            //Branching
            if (val == "Donald") // Är det Donald
            {
                Console.WriteLine("Donald was here");
            }
            else if (val == "Nisse") // Är Det Nisse
            {
                Console.WriteLine("Nisse was here");
            }
            else // DEt är ingen av Nisse eller Donald
            {
                Console.WriteLine("Trumpelitrump was here");
            }

            // Här närmar sig slutet på programmet
            Console.WriteLine("Här slutar programmet");
            Console.ReadKey();
            // Exit
        }
    }
}
