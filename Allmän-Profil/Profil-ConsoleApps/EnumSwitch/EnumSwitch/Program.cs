﻿using System;

namespace EnumSwitch
{
    class Program
    {
        // Vår nya enum
        enum State
        {
            rum1, 
            rum2,
            rum3
        }
        
        static void Main(string[] args)
        {

            // Switch för en enum
            State status = State.rum1;
            switch (status)
            {
                case State.rum1:
                    Console.WriteLine("Du är i rum 1");
                    break;
                case State.rum2:
                    Console.WriteLine("Du är i rum 2");
                    break;
                case State.rum3:
                    Console.WriteLine("Du är i rum 3");
                    break;
                default:
                    break;
            }

        }
    }
}
