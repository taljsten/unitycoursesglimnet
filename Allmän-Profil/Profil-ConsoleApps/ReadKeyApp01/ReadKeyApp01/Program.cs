﻿using System;

namespace ReadKeyApp01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ta emot ett tangenttryck
            Console.Write("Give me a key : ");
            ConsoleKeyInfo tangent = Console.ReadKey();
            Console.WriteLine();  // Formattera så att det ser snyggt ut         
            
            // Kolla om "rätt" tangent blev nedtryckt
            if (tangent.Key == ConsoleKey.A)
            {
                Console.WriteLine("Du skrev A");
            }
            else if (tangent.Key == ConsoleKey.B)
            {
                Console.WriteLine("Du skrev B");
            }
            
            Console.ReadLine();
            //Exit
        }
    }
}
