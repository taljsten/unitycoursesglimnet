﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    [SerializeField] private float changeDirTime = 2;
    [SerializeField] private int damage = 25;
    [SerializeField] private Player player;
    [SerializeField] private float attackDistance = 5;
    //[SerializeField] private Transform back;
    //[SerializeField] private Transform front;
    private float direction = 1;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = changeDirTime;
    }

    // Update is called once per frame
    void Update()
    {
        

        transform.position += Vector3.forward * direction * speed * Time.deltaTime;

        if (Vector3.Distance(transform.position, player.transform.position) < attackDistance)
        {
            Vector3 dist = player.transform.position - transform.position;

            if (dist.z < 0)
            {
                direction = -1; 
            }
            else
            {
                direction = 1;
            }
        }
        else
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                direction = -direction; // om dir = 1 blir dir =  -1 och tvärtom
                timer = changeDirTime;
            }
        }
        
        //if (transform.position.z > front.position.z)
        //{
        //    direction = -1;
        //}

        //if (transform.position.z < back.position.z)
        //{
        //    direction = 1;
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player.Health -= damage;
            Debug.Log(Player.Health);
        }
    }
}
