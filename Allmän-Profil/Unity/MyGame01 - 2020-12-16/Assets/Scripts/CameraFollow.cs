﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float smoothness = 2f;
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {        
        //transform.position = playerTransform.position;
        transform.position = Vector3.Lerp(transform.position, playerTransform.position, smoothness * Time.deltaTime);
    }
}
