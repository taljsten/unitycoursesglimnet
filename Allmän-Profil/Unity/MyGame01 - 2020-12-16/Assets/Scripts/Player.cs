﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 5.0f;
    public float jump = 2f;
    Rigidbody rb;
    private bool isOnGround;

    public static int Score { get; set; } = 0;
    public static int Health { get; set; } = 100;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isOnGround = false;
        //Score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.back * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
        }
        
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround) // ||, &&, ==, !=, >, <, >=, <= 
        {
            isOnGround = false;
            rb.AddForce(Vector3.up * jump);
        }

        if (Health <=0)
        {
            //Destroy(gameObject);
            //gameObject.SetActive(false);
            Time.timeScale = 0;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        isOnGround = true;
    }

    //private void OnCollisionStay(Collision collision)
    //{
    //    if (transform.position.y - transform.localScale.y / 2 <= collision.transform.position.y + collision.transform.localScale.y / 1.99999 &&
    //    transform.position.y + transform.localScale.y / 2 > collision.transform.position.y - collision.transform.localScale.y / 2 &&
    //    transform.position.z < collision.transform.position.z + collision.transform.localScale.z / 2 &&
    //    transform.position.z > collision.transform.position.z - collision.transform.localScale.z / 2)
    //    {
    //        isOnGround = true;
    //    }
    //}

}
