﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeadsUpDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI healthText;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "S " + Player.Score;
        healthText.text = "H " + Player.Health;
    }
}