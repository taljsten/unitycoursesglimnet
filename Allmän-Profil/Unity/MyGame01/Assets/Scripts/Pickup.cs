﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [SerializeField] private float rotSpeed = 45;
    [SerializeField] private int score = 10;

    private void Update()
    {
        transform.Rotate(Vector3.up, rotSpeed * Time.deltaTime, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player.Score += score;
            Destroy(gameObject);
        }
    }

}
