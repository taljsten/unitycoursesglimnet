﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snake.orm_mar
{
    public enum GameState
    {
        MainMenu,
        playing,
        Gameover
    }
    class Program
    {
        static void Main(string[] args)
        {

            bool isRunning = true;
            GameState gameState = GameState.MainMenu;
            Console.CursorVisible = false;
            ConsoleKeyInfo keyinfo;

            Console.WindowHeight = 32;
            Console.WindowWidth = 64;
            int screenwidth = Console.WindowWidth;
            int screenheight = Console.WindowHeight;
            Random randomnummer = new Random();
            int score = 5;
            int gameover = 0;
            pixel bella = new pixel();
            bella.xpos = screenwidth / 2;
            bella.ypos = screenheight / 2;
            bella.schermkleur = ConsoleColor.Red;
            string movement = "RIGHT";
            List<int> xpos = new List<int>();
            List<int> ypos = new List<int>();
            int berryx = randomnummer.Next(0, screenwidth);
            int berryy = randomnummer.Next(0, screenheight);
            DateTime tijd = DateTime.Now;
            DateTime tijd2 = DateTime.Now;
            string buttonpressed = "no";

            while (isRunning)
            {
                switch (gameState)
                {
                    case GameState.MainMenu:
                        Console.SetCursorPosition(5, 10);
                        Console.WriteLine("Welcome to Snake Game");
                        Console.Read();
                        gameState = GameState.playing;
                        break;
                    case GameState.playing:
                        while (true)
                        {
                            Console.Clear();
                            if (bella.xpos == screenwidth - 1 || bella.xpos == 0 || bella.ypos == screenheight - 1 || bella.ypos == 0)
                            {
                                gameover = 1;
                            }
                            for (int i = 0; i < screenwidth; i++)
                            {
                                Console.SetCursorPosition(i, 0);
                                Console.Write("■");
                            }
                            for (int i = 0; i < screenwidth; i++)
                            {
                                Console.SetCursorPosition(i, screenheight - 1);
                                Console.Write("■");
                            }
                            for (int i = 0; i < screenheight; i++)
                            {
                                Console.SetCursorPosition(0, i);
                                Console.Write("■");
                            }
                            for (int i = 0; i < screenheight; i++)
                            {
                                Console.SetCursorPosition(screenwidth - 1, i);
                                Console.Write("■");
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            if (berryx == bella.xpos && berryy == bella.ypos)
                            {
                                score++;
                                berryx = randomnummer.Next(1, screenwidth - 2);
                                berryy = randomnummer.Next(1, screenheight - 2);
                            }
                            for (int i = 0; i < xpos.Count(); i++)
                            {
                                Console.SetCursorPosition(xpos[i], ypos[i]);
                                Console.Write("■");
                                if (xpos[i] == bella.xpos && ypos[i] == bella.ypos)
                                {
                                    gameover = 1;
                                }
                            }
                            if (gameover == 1)
                            {
                                break;
                            }
                            Console.SetCursorPosition(bella.xpos, bella.ypos);
                            Console.ForegroundColor = bella.schermkleur;
                            Console.Write("■");
                            Console.SetCursorPosition(berryx, berryy);
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("■");
                            tijd = DateTime.Now;
                            buttonpressed = "no";
                            while (true)
                            {
                                tijd2 = DateTime.Now;
                                if (tijd2.Subtract(tijd).TotalMilliseconds > 500) { break; }
                                if (Console.KeyAvailable)
                                {
                                    ConsoleKeyInfo toets = Console.ReadKey(true);

                                    if (toets.Key.Equals(ConsoleKey.UpArrow) && movement != "DOWN" && buttonpressed == "no")
                                    {
                                        movement = "UP";
                                        buttonpressed = "yes";
                                    }
                                    if (toets.Key.Equals(ConsoleKey.DownArrow) && movement != "UP" && buttonpressed == "no")
                                    {
                                        movement = "DOWN";
                                        buttonpressed = "yes";
                                    }
                                    if (toets.Key.Equals(ConsoleKey.LeftArrow) && movement != "RIGHT" && buttonpressed == "no")
                                    {
                                        movement = "LEFT";
                                        buttonpressed = "yes";
                                    }
                                    if (toets.Key.Equals(ConsoleKey.RightArrow) && movement != "LEFT" && buttonpressed == "no")
                                    {
                                        movement = "RIGHT";
                                        buttonpressed = "yes";
                                    }
                                }
                            }
                            xpos.Add(bella.xpos);
                            ypos.Add(bella.ypos);
                            switch (movement)
                            {
                                case "UP":
                                    bella.ypos--;
                                    break;
                                case "DOWN":
                                    bella.ypos++;
                                    break;
                                case "LEFT":
                                    bella.xpos--;
                                    break;
                                case "RIGHT":
                                    bella.xpos++;
                                    break;
                            }
                            if (xpos.Count() > score)
                            {
                                xpos.RemoveAt(0);
                                ypos.RemoveAt(0);
                            }
                            break;

                        }
                        break;

                    case GameState.Gameover:
                        Console.SetCursorPosition(screenwidth / 5, screenheight / 2);
                        Console.WriteLine("Game over, Score: " + score);
                        Console.SetCursorPosition(screenwidth / 5, screenheight / 2 + 1);
                        break;
                    default:
                        break;

                }


               
            }
           


            

        }

        class pixel
        {
            public int xpos { get; set; }
            public int ypos { get; set; }
            public ConsoleColor schermkleur { get; set; }
        }
    }
}



