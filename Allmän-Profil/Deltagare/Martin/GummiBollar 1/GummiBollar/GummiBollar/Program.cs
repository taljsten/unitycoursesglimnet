﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GummiBollar
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            bool isPlaying = false;
            Console.CursorVisible = false;
            Random rng = new Random();
            int antalGummibollar = rng.Next(1, 1000);

            while (isRunning)
            {
                if (isPlaying == false)
                {
                    Console.SetCursorPosition(10, 10);
                    Console.Write("Hej och välkommen till Gummibollar!");
                    Console.SetCursorPosition(10, 12);
                    Console.Write("Meny: ");
                    Console.SetCursorPosition(11, 13);
                    Console.Write("Tryck F1. för att börja spelet.");
                    Console.SetCursorPosition(11, 14);
                    Console.Write("Tryck F2. för att avsluta spelet.");

                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.F1)
                    {
                        isPlaying = true;
                        Console.Clear();
                    }
                    else if (keyInfo.Key == ConsoleKey.F2)
                    {
                        isRunning = false;
                    }
                }



                while (isRunning)
                {
                    Console.Clear();
                    Console.Write("Gissa hur många gummibollar jag har i min burk:");
                    string gissaString = Console.ReadLine();
                    int gissa = int.Parse(gissaString);



                    if (gissa > antalGummibollar)

                    {
                        Console.WriteLine("För högt, gissa igen.");
                        Thread.Sleep(1500);
                    }

                    if (gissa < antalGummibollar)

                    {
                        Console.WriteLine("För lågt, gissa igen.");
                        Thread.Sleep(1500);
                    }

                    if (gissa == antalGummibollar)

                    {
                        Console.WriteLine("HURRA DET VAR RÄTT!!!");
                        Thread.Sleep(1500);
                        isRunning = false;
                    }

                }
                Console.Clear();
                Console.WriteLine("SPEL SLUT");
                Console.ReadLine();

            }
        }
    }
}


           
       

       

   



