﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace GameProject01
{
    public enum GameState
    {
        MainMenu,
        Playing,
        GameOver,
        Reset,
        load

    }
    public class Room
    {
        public int minXroom;
        public int minYroom;
        public int maxXroom;
        public int maxYroom;
        public Room(int minxroom, int minyroom, int maxxroom, int maxyroom)
        {
            minYroom = minxroom;
            minYroom = minyroom;
            maxXroom = maxxroom;
            maxYroom = maxyroom;
        }
    }
    public class Coord
    {
        public int xPos = 0;
        public int yPos = 0;
        public int xRoom = 0;
        public int yRoom = 0;
        public Coord(int xpos, int ypos, int xroom, int yroom)
        {
            xPos = xpos;
            yPos = ypos;
            xRoom = xroom;
            yRoom = yroom;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Coord> Coordanites = new List<Coord>();
            Console.CursorSize = 1;
            Console.WindowWidth = 64;
            Console.WindowHeight = 32;
            Console.BufferHeight = Console.WindowHeight;
            Console.BufferWidth = Console.WindowWidth;
            int windowWidth = Console.BufferWidth - 1;
            int windowHeigth = Console.WindowHeight - 10;
            int scoreBoard = 0;
            int flickerMenu = 0;
            int selectorMenu = 3;
            Random rng = new Random();
            List<Coord> ObstaclePos = new List<Coord>();
            Coord CurrentPos = new Coord((rng.Next(1, windowWidth)), (windowHeigth / 2), 0, 0);
            Random rngBerry = new Random();
            List<string> playernames = new List<string>();
            List<int> scorepoints = new List<int>();
            int xMomentum = 0;
            int yMomentum = 1;
            //Direction movement = Direction.Vertical;
            int refreshRate = 150;
            Console.CursorVisible = false;
            //bool berryAlive = new bool[]
            bool berryAlive = false;
            bool isRunning = true;
            Coord berryPos = new Coord(0, 0, 0, 0);
            GameState GameIsState = GameState.MainMenu;

            bool alive = false;
            string scoreBoardString = "Score: 0";
            string optionsString = "Options";
            string startString = "Start Game";
            string playername = null;
            //bool onceTime = true;
            string line = "";
            for (int i = 0; i < 10; i++)
            {
                Coordanites.Add(new Coord(CurrentPos.xPos, (CurrentPos.yPos - (1 - i)), 0, 0));
            }
            using (StreamReader sr = new StreamReader("ScoreToplist.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                {

                    if (true)
                    {

                    }
                }
            }
            while (isRunning)
            {
                if (GameIsState == GameState.Reset)
                {
                    scoreBoard = 0;
                    startString = "Start Game";
                    scoreBoardString = "Score: 0";
                    refreshRate = 150;
                    berryAlive = false;
                    Coordanites.Clear();
                    CurrentPos = new Coord((rng.Next(1, windowWidth)), (windowHeigth / 2), 0, 0);
                    yMomentum = 1;
                    xMomentum = 0;

                    alive = true;
                    for (int i = 0; i < 10; i++)
                    {
                        Coordanites.Add(new Coord(CurrentPos.xPos, (CurrentPos.yPos - (1 + i)), 0, 0));

                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 10; i++)
                            {
                                ObstaclePos.Add(new Coord(i + 3, 3, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 10; i++)
                            {
                                ObstaclePos.Add(new Coord(windowWidth - i - 3, windowHeigth - 3, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 10; i++)
                            {
                                ObstaclePos.Add(new Coord(windowWidth - i - 3,  3, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 10; i++)
                            {
                                ObstaclePos.Add(new Coord(i + 3, windowHeigth - 3, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                ObstaclePos.Add(new Coord(3, windowHeigth - 3 - i, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                ObstaclePos.Add(new Coord(windowWidth - 3, windowHeigth - 3 - i, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                ObstaclePos.Add(new Coord(3, 3 + i, k, d));
                            }
                        }
                    }
                    for (int d = -1; d < 2; d++)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                ObstaclePos.Add(new Coord(windowWidth - 3, 3 + i, k, d));
                            }
                        }
                    }



                    Console.Write("Your player name: ");
                    playername = Console.ReadLine();
                    Console.Clear();
                    GameIsState = GameState.Playing;
                }
                if (GameIsState == GameState.MainMenu)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    string quitString = "ESC to quit";
                    Console.CursorLeft = (Console.WindowWidth / 2) - (quitString.Length / 2);
                    Console.CursorTop = (Console.WindowHeight) - 3;
                    Console.Write(quitString);

                    if (selectorMenu == 3)
                    {
                        Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                        Console.CursorTop = (Console.WindowHeight / 2) - 3;
                        Console.Write(">");
                        if (flickerMenu == 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Thread.Sleep(250);
                            flickerMenu = 0;
                        }
                        else if (flickerMenu == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Thread.Sleep(250);
                            flickerMenu = 1;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2);
                    Console.CursorTop = (Console.WindowHeight / 2) - 3;
                    Console.Write(startString);
                    string loadString = "Load";
                    if (selectorMenu == 2)
                    {
                        Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                        Console.CursorTop = (Console.WindowHeight / 2) - 2;
                        Console.Write(">");
                        if (flickerMenu == 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Thread.Sleep(250);
                            flickerMenu = 0;
                        }
                        else if (flickerMenu == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Thread.Sleep(250);
                            flickerMenu = 1;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.CursorLeft = (Console.WindowWidth / 2) - (loadString.Length / 2);
                    Console.CursorTop = (Console.WindowHeight / 2) - 2;
                    Console.Write(loadString);


                    if (selectorMenu == 1)
                    {
                        Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                        Console.CursorTop = (Console.WindowHeight / 2) - 1;
                        Console.Write(">");
                        if (flickerMenu == 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;

                            flickerMenu = 0;
                            Thread.Sleep(250);
                        }
                        else if (flickerMenu == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.White;

                            flickerMenu = 1;
                            Thread.Sleep(250);
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.CursorLeft = (Console.WindowWidth / 2) - (optionsString.Length / 2);
                    Console.CursorTop = (Console.WindowHeight / 2) - 1;
                    Console.Write(optionsString);
                    string creditsString = "Credits";
                    if (selectorMenu == 0)
                    {
                        Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                        Console.CursorTop = (Console.WindowHeight / 2);
                        Console.Write(">");
                        if (flickerMenu == 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Thread.Sleep(250);
                            flickerMenu = 0;
                        }
                        else if (flickerMenu == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Thread.Sleep(250);
                            flickerMenu = 1;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.CursorLeft = (Console.WindowWidth / 2) - (creditsString.Length / 2);
                    Console.CursorTop = (Console.WindowHeight / 2);
                    Console.Write(creditsString);
                    //for (int i = 1; i < 5; i++)
                    //{
                    //    if (i == selectorMenu)
                    //    {
                    //        i++;
                    //    }
                    //    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                    //    Console.CursorTop = (Console.WindowHeight / 2) - i;
                    //    Console.Write(' ');
                    //}

                    if (Console.KeyAvailable)
                    {
                        switch (Console.ReadKey(true).Key)
                        {
                            case ConsoleKey.UpArrow:
                                if (selectorMenu == 3)
                                {
                                    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                                    Console.CursorTop = (Console.WindowHeight / 2) - 3;
                                    Console.Write(' ');
                                    selectorMenu = 0;
                                }
                                else if (selectorMenu <= 3 && selectorMenu >= 0)
                                {
                                    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                                    Console.CursorTop = (Console.WindowHeight / 2) - selectorMenu;
                                    Console.Write(' ');
                                    selectorMenu++;
                                }
                                break;
                            case ConsoleKey.DownArrow:
                                if (selectorMenu == 0)
                                {
                                    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                                    Console.CursorTop = (Console.WindowHeight / 2);
                                    Console.Write(' ');
                                    selectorMenu = 3;
                                }
                                else if (selectorMenu <= 3 && selectorMenu >= 0)
                                {
                                    Console.CursorLeft = (Console.WindowWidth / 2) - (startString.Length / 2) - 1;
                                    Console.CursorTop = (Console.WindowHeight / 2) - selectorMenu;
                                    Console.Write(' ');
                                    selectorMenu--;
                                }
                                break;
                            case ConsoleKey.Enter:
                                if (selectorMenu == 3)
                                {

                                    if (alive != true)
                                    {
                                        GameIsState = GameState.Reset;
                                    }
                                    else
                                    {
                                        GameIsState = GameState.Playing;
                                    }

                                    Console.Clear();
                                }
                                break;
                            case ConsoleKey.Escape:
                                isRunning = false;
                                break;
                            default:
                                break;
                        }
                    }
                }
                if (GameIsState == GameState.Playing)
                {
                    if (alive == false)
                    {
                        string deadString = "You died";
                        Console.CursorLeft = (Console.WindowWidth / 2) - (deadString.Length / 2);
                        Console.CursorTop = (Console.WindowHeight / 2) - 4;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(deadString);
                        Thread.Sleep(5000);
                        Console.Clear();
                        GameIsState = GameState.MainMenu;
                    }
                    if (alive)
                    {

                        //if (onceTime)
                        //{
                        //    for (int j = 0; j < windowHeigth; j++)
                        //    {
                        //        for (int i = 0; i < windowWidth; i++)
                        //        {
                        //            if ((i % 2) != (j % 2))
                        //            {
                        //                Console.ForegroundColor = ConsoleColor.DarkGray;
                        //            }
                        //            else
                        //            {
                        //                Console.ForegroundColor = ConsoleColor.White;
                        //            }
                        //            for (int k = snakeXpos.Count() - 1; k > 0; k--)
                        //            {
                        //                if (i != snakeXpos[k] && j != snakeYpos[k])
                        //                {
                        //                    Console.SetCursorPosition(i, j);
                        //                    Console.Write('■');
                        //                }
                        //            }

                        //        }
                        //    }
                        //}
                        //onceTime = false;


                        if (Console.KeyAvailable)
                        {
                            switch (Console.ReadKey(true).Key)
                            {
                                case ConsoleKey.UpArrow:
                                    if (xMomentum != 0)
                                    {
                                        yMomentum = -1;
                                        xMomentum = 0;
                                    }
                                    break;
                                case ConsoleKey.DownArrow:
                                    if (xMomentum != 0)
                                    {
                                        yMomentum = 1;
                                        xMomentum = 0;
                                    }
                                    break;
                                case ConsoleKey.LeftArrow:
                                    if (yMomentum != 0)
                                    {
                                        xMomentum = -1;
                                        yMomentum = 0;
                                    }
                                    break;
                                case ConsoleKey.RightArrow:
                                    if (yMomentum != 0)
                                    {
                                        xMomentum = 1;
                                        yMomentum = 0;
                                    }
                                    break;
                                case ConsoleKey.Escape:
                                    startString = "Continue";
                                    GameIsState = GameState.MainMenu;
                                    Console.Clear();
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (Coordanites[Coordanites.Count() - 1].xRoom == CurrentPos.xRoom && Coordanites[Coordanites.Count() - 1].yRoom == CurrentPos.yRoom)
                        {
                            Console.SetCursorPosition(Coordanites[Coordanites.Count() - 1].xPos, Coordanites[Coordanites.Count() - 1].yPos);
                            Console.Write(' ');
                        }
                        Coordanites[0].xPos = CurrentPos.xPos;
                        Coordanites[0].yPos = CurrentPos.yPos;
                        Coordanites[0].xRoom = CurrentPos.xRoom;
                        Coordanites[0].yRoom = CurrentPos.yRoom;
                        CurrentPos.xPos += xMomentum;
                        CurrentPos.yPos += yMomentum;
                        if (xMomentum != 0 || yMomentum != 0)
                        {
                            if (xMomentum != 0)
                            {
                                if (CurrentPos.xPos < 0)
                                {
                                    CurrentPos.xPos = windowWidth;
                                    CurrentPos.xRoom--;
                                    Console.Clear();
                                }
                                else if (CurrentPos.xPos > windowWidth)
                                {
                                    CurrentPos.xPos = 1;
                                    CurrentPos.xRoom++;
                                    Console.Clear();
                                }
                            }
                            if (yMomentum != 0)
                            {
                                if (CurrentPos.yPos < 0)
                                {
                                    CurrentPos.yPos = windowHeigth;
                                    CurrentPos.yRoom++;
                                    Console.Clear();
                                }
                                else if (CurrentPos.yPos > windowHeigth)
                                {
                                    CurrentPos.yPos = 1;
                                    CurrentPos.yRoom--;
                                    Console.Clear();
                                }
                            }

                            for (int i = Coordanites.Count() - 1; i > 0; i--)
                            {

                                Coordanites[i].xPos = Coordanites[i - 1].xPos;
                                Coordanites[i].yPos = Coordanites[i - 1].yPos;
                                Coordanites[i].xRoom = Coordanites[i - 1].xRoom;
                                Coordanites[i].yRoom = Coordanites[i - 1].yRoom;



                            }





                            Console.SetCursorPosition(CurrentPos.xPos, CurrentPos.yPos);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("■");
                            if (berryPos.xPos == CurrentPos.xPos && berryPos.yPos == CurrentPos.yPos)
                            {
                                if ((Coordanites[Coordanites.Count() - 1].xPos + xMomentum) < 0)
                                {
                                    Coordanites.Add(new Coord(windowWidth, (Coordanites[Coordanites.Count() - 1].yPos + yMomentum), (Coordanites[Coordanites.Count() - 1].xRoom - 1), Coordanites[Coordanites.Count() - 1].yRoom));
                                    Coordanites.Add(new Coord(windowWidth, (Coordanites[Coordanites.Count() - 1].yPos + yMomentum), (Coordanites[Coordanites.Count() - 1].xRoom - 1), Coordanites[Coordanites.Count() - 1].yRoom));

                                }
                                if ((Coordanites[Coordanites.Count() - 1].xPos + xMomentum) > windowWidth)
                                {
                                    Coordanites.Add(new Coord(0, (Coordanites[Coordanites.Count() - 1].yPos + yMomentum), (Coordanites[Coordanites.Count() - 1].xRoom + 1), Coordanites[Coordanites.Count() - 1].yRoom));

                                }
                                if ((Coordanites[Coordanites.Count() - 1].yPos + yMomentum) < 0)
                                {
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, windowHeigth, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom + 1));
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, windowHeigth, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom + 1));

                                }
                                if ((Coordanites[Coordanites.Count() - 1].yPos + yMomentum) > windowHeigth)
                                {
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, 0, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom));
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, 0, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom));
                                }
                                if ((Coordanites[Coordanites.Count() - 1].xPos + xMomentum) > 0 && (Coordanites[Coordanites.Count() - 1].xPos + xMomentum) < windowWidth && (Coordanites[Coordanites.Count() - 1].yPos + yMomentum) > 0 && (Coordanites[Coordanites.Count() - 1].yPos + yMomentum) < windowHeigth)
                                {
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, Coordanites[Coordanites.Count() - 1].yPos + yMomentum, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom));
                                    Coordanites.Add(new Coord(Coordanites[Coordanites.Count() - 1].xPos + xMomentum, Coordanites[Coordanites.Count() - 1].yPos + yMomentum, Coordanites[Coordanites.Count() - 1].xRoom, Coordanites[Coordanites.Count() - 1].yRoom));

                                }
                                if (refreshRate > 50)
                                {
                                    refreshRate -= 5;
                                }
                                scoreBoard += 10;
                                scoreBoardString = "score: " + scoreBoard;



                                berryAlive = false;
                            }
                            if (berryAlive == false)
                            {
                                Console.SetCursorPosition(berryPos.xPos, berryPos.yPos);
                                Console.Write(' ');
                                for (int i = 0; i < (Coordanites.Count() - 1); i++)
                                {
                                    berryPos.xPos = rngBerry.Next(1, windowWidth);
                                    berryPos.yPos = rngBerry.Next(1, windowHeigth);
                                    berryPos.xRoom = rngBerry.Next(-1, 1);
                                    berryPos.yRoom = rngBerry.Next(-1, 1);
                                    if (berryPos.xPos != Coordanites[i].xPos && berryPos.yPos != Coordanites[i].yPos)
                                    {
                                        i = Coordanites.Count() - 1;
                                        berryAlive = true;

                                    }
                                }


                            }
                            if (berryAlive == true)
                            {
                                if (berryPos.xRoom == CurrentPos.xRoom && berryPos.yRoom == CurrentPos.yRoom)
                                {
                                    Console.SetCursorPosition(berryPos.xPos, berryPos.yPos);
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.Write("■");
                                }
                                else
                                {
                                    if (berryPos.xRoom < CurrentPos.xRoom)
                                    {
                                        Console.SetCursorPosition(0, windowHeigth / 2);
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.Write("!");
                                    }
                                    if (berryPos.xRoom > CurrentPos.xRoom)
                                    {
                                        Console.SetCursorPosition(windowWidth - 1, windowHeigth / 2);
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.Write("!");
                                    }
                                    if (berryPos.yRoom < CurrentPos.yRoom)
                                    {
                                        Console.SetCursorPosition(windowWidth/2, windowHeigth - 1);
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.Write("!");
                                    }
                                    if (berryPos.yRoom > CurrentPos.yRoom)
                                    {
                                        Console.SetCursorPosition(windowWidth/2, 0);
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.Write("!");
                                    }
                                }
                            }
                            for (int i = 0; i < ObstaclePos.Count() - 1; i++)
                            {
                                if (ObstaclePos[i].xRoom == CurrentPos.xRoom && ObstaclePos[i].yRoom == CurrentPos.yRoom)
                                {
                                    Console.SetCursorPosition(ObstaclePos[i].xPos, ObstaclePos[i].yPos);
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    Console.Write("■");
                                }

                            }
                            for (int i = 0; i < (Console.WindowWidth); i++)
                            {
                                Console.SetCursorPosition(i, windowHeigth + 1);
                                Console.ForegroundColor = ConsoleColor.Gray;
                                Console.Write("■");
                            }
                            Console.SetCursorPosition((Console.WindowWidth - scoreBoardString.Length), Console.WindowHeight - 2);
                            Console.Write(scoreBoardString);
                            for (int i = 0; i < ObstaclePos.Count() - 1; i++)
                            {
                                if (ObstaclePos[i].xPos == CurrentPos.xPos && ObstaclePos[i].yPos == CurrentPos.yPos && CurrentPos.yRoom == ObstaclePos[i].yRoom && CurrentPos.xRoom == ObstaclePos[i].xRoom)
                                {
                                    alive = false;
                                }
                            }
                            for (int i = Coordanites.Count() - 1; i > 0; i--)
                            {
                                if (Coordanites[i].xPos == CurrentPos.xPos && Coordanites[i].yPos == CurrentPos.yPos && CurrentPos.yRoom == Coordanites[i].yRoom && CurrentPos.xRoom == Coordanites[i].xRoom)
                                {
                                    alive = false;
                                    scorepoints.Add(scoreBoard);
                                    playernames.Add(playername);
                                }
                                if (Coordanites[i].xRoom == CurrentPos.xRoom && Coordanites[i].yRoom == CurrentPos.yRoom)
                                {


                                    Console.SetCursorPosition(Coordanites[i].xPos, Coordanites[i].yPos);
                                    Console.Write(' ');

                                    Console.SetCursorPosition(Coordanites[i].xPos, Coordanites[i].yPos);
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write("■");
                                }
                            }
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.SetCursorPosition(0, Console.WindowHeight - 1);
                            Console.Write(CurrentPos.xRoom + "x, " + CurrentPos.yRoom + "y");
                            Thread.Sleep(refreshRate);




                            for (int i = 0; i < Coordanites.Count(); i++)
                            {
                                Debug.WriteLine(Coordanites[i]);
                            }

                            //Debug.WriteLine((snakeXpos[snakeXpos.Count() - 1] % 2) +  " " + (snakeYpos[snakeYpos.Count() - 1] % 2));
                            //if ((snakeXpos[snakeXpos.Count() - 1] % 2) != (snakeYpos[snakeYpos.Count() - 1] % 2))
                            //{
                            //    Console.ForegroundColor = ConsoleColor.DarkGray;
                            //}
                            //else
                            //{
                            //    Console.ForegroundColor = ConsoleColor.White;
                            //}
                            //Console.Write("■");

                            //Console.Clear();


                        }
                    }
                }


            }
            using (StreamWriter savefile = new StreamWriter("ScoreToplist.txt"))
            {
                for (int i = 0; i < playernames.Count(); i++)
                {
                    savefile.WriteLine("Name:" + playernames[i]);
                    savefile.WriteLine("Score:" + scorepoints[i]);
                }
            }
        }
    }
}
